/// @description Menu Visual
// You can write your code in this editor
var i = 0;
repeat (buttons){
	draw_set_font (font_main); //text font
	draw_set_halign(fa_center);
	draw_set_color(c_gray) //default menu color
	
	if (menu_index == i) { //selected option color
		draw_set_color(c_red);
		}
		
	draw_text(menu_x, menu_y + button_h * i, button [i]);
	i++
}