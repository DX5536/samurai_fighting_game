/// @description Init
// You can write your code in this editor

control_x = room_width/2;
control_y = room_height/4;
button_h = 32;

//buttons
control_button[0] = "A: Move Left"
control_button[1] = "D: Move Right"
control_button[2] = "F: Slash"
control_button[3] = "R: Thrust"
control_button[4] = "W: Jump Slam"

control_buttons = array_length_1d(control_button);

control_index = 0;