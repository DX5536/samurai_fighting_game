/// @description Samurai movement
// You can write your code in this editor

if (keyboard_check(ord("D"))) currentSpeed = walkSpeed;
else if (keyboard_check(ord("A"))) currentSpeed = -walkSpeed;
else currentSpeed = 0;

//x = x + currentSpeed;
x = clamp(x+currentSpeed,0,obj_samurai_2.x)

//attack animation
slash = keyboard_check(ord("F"))
thrust = keyboard_check(ord("R"))
jumpslam = keyboard_check(ord("W"))

if (slash == true) sprite_index = spr_samurai_1_slash;
else if (thrust == true) sprite_index = spr_samurai_1_thrust;
else if (jumpslam == true) sprite_index = spr_samurai_1_jumpslam;

else sprite_index = spr_samurai_1;


