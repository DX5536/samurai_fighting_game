{
  "bboxMode": 0,
  "collisionKind": 4,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 40,
  "bbox_right": 89,
  "bbox_top": 0,
  "bbox_bottom": 63,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 90,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"003d9988-b506-4404-bab4-d74097060fe8","path":"sprites/spr_samurai_1_slash_hitbox/spr_samurai_1_slash_hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"003d9988-b506-4404-bab4-d74097060fe8","path":"sprites/spr_samurai_1_slash_hitbox/spr_samurai_1_slash_hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"003d9988-b506-4404-bab4-d74097060fe8","path":"sprites/spr_samurai_1_slash_hitbox/spr_samurai_1_slash_hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"003d9988-b506-4404-bab4-d74097060fe8","path":"sprites/spr_samurai_1_slash_hitbox/spr_samurai_1_slash_hitbox.yy",},"LayerId":{"name":"spr_samurai_1_slash_hitbox","path":"sprites/spr_samurai_1_slash_hitbox/spr_samurai_1_slash_hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"003d9988-b506-4404-bab4-d74097060fe8","path":"sprites/spr_samurai_1_slash_hitbox/spr_samurai_1_slash_hitbox.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"003d9988-b506-4404-bab4-d74097060fe8","path":"sprites/spr_samurai_1_slash_hitbox/spr_samurai_1_slash_hitbox.yy",},"LayerId":{"name":"a06a8886-adb8-44d1-8ba8-2df4f8204b8e","path":"sprites/spr_samurai_1_slash_hitbox/spr_samurai_1_slash_hitbox.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_samurai_1_slash_hitbox","path":"sprites/spr_samurai_1_slash_hitbox/spr_samurai_1_slash_hitbox.yy",},"resourceVersion":"1.0","name":"003d9988-b506-4404-bab4-d74097060fe8","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_samurai_1_slash_hitbox","path":"sprites/spr_samurai_1_slash_hitbox/spr_samurai_1_slash_hitbox.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 4.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"15cb0263-6b14-454a-8835-9de60b9793ec","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"003d9988-b506-4404-bab4-d74097060fe8","path":"sprites/spr_samurai_1_slash_hitbox/spr_samurai_1_slash_hitbox.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_samurai_1_slash_hitbox","path":"sprites/spr_samurai_1_slash_hitbox/spr_samurai_1_slash_hitbox.yy",},
    "resourceVersion": "1.3",
    "name": "spr_samurai_1_slash_hitbox",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":70.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"a06a8886-adb8-44d1-8ba8-2df4f8204b8e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Samurai_1",
    "path": "folders/Sprites/Samurai_1.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_samurai_1_slash_hitbox",
  "tags": [],
  "resourceType": "GMSprite",
}