{
  "bboxMode": 2,
  "collisionKind": 4,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 113,
  "bbox_right": 168,
  "bbox_top": 3,
  "bbox_bottom": 62,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 200,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"214cfa34-a825-4ac8-921d-80b2dc9d8479","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"214cfa34-a825-4ac8-921d-80b2dc9d8479","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"LayerId":{"name":"092eed27-c1fe-4034-8173-a33d97a21a7a","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"214cfa34-a825-4ac8-921d-80b2dc9d8479","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"214cfa34-a825-4ac8-921d-80b2dc9d8479","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"214cfa34-a825-4ac8-921d-80b2dc9d8479","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"214cfa34-a825-4ac8-921d-80b2dc9d8479","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"214cfa34-a825-4ac8-921d-80b2dc9d8479","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"214cfa34-a825-4ac8-921d-80b2dc9d8479","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"214cfa34-a825-4ac8-921d-80b2dc9d8479","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"LayerId":{"name":"e34e2fae-ddb0-4e20-bda9-88480ad31108","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_samurai_1_thrust","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"resourceVersion":"1.0","name":"214cfa34-a825-4ac8-921d-80b2dc9d8479","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1c2b02aa-2788-47c5-877d-8e7cf4f198d2","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1c2b02aa-2788-47c5-877d-8e7cf4f198d2","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"LayerId":{"name":"e34e2fae-ddb0-4e20-bda9-88480ad31108","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"1c2b02aa-2788-47c5-877d-8e7cf4f198d2","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"LayerId":{"name":"092eed27-c1fe-4034-8173-a33d97a21a7a","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_samurai_1_thrust","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"resourceVersion":"1.0","name":"1c2b02aa-2788-47c5-877d-8e7cf4f198d2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"67a1f1c6-3356-41c6-8322-f03dcfb65bd9","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"67a1f1c6-3356-41c6-8322-f03dcfb65bd9","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"LayerId":{"name":"e34e2fae-ddb0-4e20-bda9-88480ad31108","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"67a1f1c6-3356-41c6-8322-f03dcfb65bd9","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"LayerId":{"name":"092eed27-c1fe-4034-8173-a33d97a21a7a","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_samurai_1_thrust","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"resourceVersion":"1.0","name":"67a1f1c6-3356-41c6-8322-f03dcfb65bd9","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_samurai_1_thrust","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 9.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 3.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"19be9d17-4999-4da0-9edd-0246a7066caa","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"214cfa34-a825-4ac8-921d-80b2dc9d8479","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"15a7bb2c-ae8f-4f3f-9724-6a451ff30798","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1c2b02aa-2788-47c5-877d-8e7cf4f198d2","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2d36a72c-ea3e-4f87-b2b7-57f7bdadb559","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"67a1f1c6-3356-41c6-8322-f03dcfb65bd9","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_samurai_1_thrust","path":"sprites/spr_samurai_1_thrust/spr_samurai_1_thrust.yy",},
    "resourceVersion": "1.3",
    "name": "spr_samurai_1_thrust",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":71.0,"displayName":"Layer 2","resourceVersion":"1.0","name":"e34e2fae-ddb0-4e20-bda9-88480ad31108","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"092eed27-c1fe-4034-8173-a33d97a21a7a","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Samurai_1",
    "path": "folders/Sprites/Samurai_1.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_samurai_1_thrust",
  "tags": [],
  "resourceType": "GMSprite",
}